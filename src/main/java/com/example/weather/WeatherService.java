package com.example.weather;

import com.example.weather.api.WeatherAPIClient;
import com.example.weather.api.WeatherAPIResponse;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.List;
@Service
public class WeatherService {
    private final WeatherRepository repository;
    private final WeatherAPIClient api;
    public WeatherService(final WeatherAPIClient api, final WeatherRepository repository){
        this.api = api;
        this.repository = repository;
    }

    public Flux<WeatherInfo> getAll(){
        return repository.findAll();
    }

    public Flux<WeatherInfo> getForCity(final String city){
        return repository
                .findAllByCityIgnoreCase(city)
                .switchIfEmpty(
                        api
                                .getWeather(city)
                                .flatMapMany(apiResponse ->
                                        repository.saveAll(apiResponse.toWeatherInfoList())
                                )
                );
    }
}
