package com.example.weather;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import java.util.List;
@RestController
@RequestMapping("/weather")
public class WeatherController {
    private final WeatherService service;

    public WeatherController(final WeatherService service){
        this.service = service;
    }
    @GetMapping
    public Flux<WeatherInfo> getAll(){
        return service.getAll();
    }
    @GetMapping("/city/{city}")
    public Flux<WeatherInfo> getForCity(@PathVariable final String city){
        System.out.println(city);
//       for(WeatherInfo info : service.getForCity(city).toIterable())
//            System.out.println(info.city() + " " + info.state() + " " + info.localDate() + " " + info.avgTemperature());
        return service.getForCity(city);
    }
}
