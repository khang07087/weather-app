package com.example.weather;
//import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.StringJoiner;
import org.springframework.data.annotation.Id;
public class WeatherInfo {
    @Id
    private Long id;
    private String region;
    private String country;
    private String state;
    private String city;
    private LocalDate localDate;
    private String avgTemperature;

    public WeatherInfo(Long id, String region, String country, String state, String city, LocalDate localDate, String avgTemperature){
        this.id = id;
        this.region = region;
        this.country = country;
        this.state = state;
        this.city = city;
        this.localDate = localDate;
        this.avgTemperature = avgTemperature;
    }
    public WeatherInfo(){
        new WeatherInfo(null, null, null, null, null, null, null);
    }
    public String city(){
        return this.city;
    }
    public String state(){
        return this.state;
    }
    public LocalDate localDate(){
        return this.localDate;
    }
    public String avgTemperature(){
        return this.avgTemperature;
    }
}
